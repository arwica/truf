<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/08/16
 * Time: 16:10
 */

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Input;
use App\Models\Truf;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class TrufGameController extends BaseController {

    public function __construct() {

    }

    public function index() {
        $data = array();
        $data['history'] = Session::get('history');

        return view('layout/index', $data);
    }

    public function trufCalculation(){
        $bids = Input::get('bids');
        $tricks = Input::get('tricks');

        $truf = new Truf($bids, $tricks);

        $results = $truf->scoreCalculation();
        if (!empty($results))
            Session::push('history', $results);

        return Redirect('/');
    }

    public function clearHistoryTruf(){
        Session::forget('history');

        Session::flush();

        return Redirect('/');
    }

}