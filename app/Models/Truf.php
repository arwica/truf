<?php
namespace App\Models;

/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/08/16
 * Time: 16:11
 */
class Truf {

    public $bids;

    public $trick;

    public function __construct(Array $bids, Array $tricks) {
        $this->bids = array(
            'n' => $bids[0],
            's' => $bids[1],
            'w' => $bids[2],
            'e' => $bids[3],
        );

        $this->trick = array(
            'n' => $tricks[0],
            's' => $tricks[1],
            'w' => $tricks[2],
            'e' => $tricks[3],
        );
    }

    public function scoreCalculation(){
        $results = array();

        if (!empty($this->bids) AND !empty($this->trick)) {
            $total_bids = $this->getTotalBids();

            foreach ($this->bids as $i => $key) {
                $t = $this->trick[$i];
                $b = $key;

                $results["point_of_" . $i] = $this->perPlayerScoreCalculation($b, $t, $total_bids);
                $results['tricks_of_' . $i] = $t;
                $results['bid_of_' . $i] = $b;
            }

            $results['truf_point'] = $this->getTrufPoint();
        }

        return $results;
    }

    public function perPlayerScoreCalculation($bids_per_player, $tricks_per_player, $total_bids){
        $results = 0;
        $diff = $this->getTotalDiff($total_bids);

        $t = $tricks_per_player;
        $b = $bids_per_player;

        if ($this->getTypeGame() == "Atas") {
            if ($b == $t) {
                $results = $t;
            } else if ($b < $t) {
                $results = 1 * -abs($t-$b);
            } else if ($b > $t) {
                $results = $diff * -abs($t-$b);
            } else if ($b == 0 AND $t == 0) {
                $results = 10;
            } else if ($b == 0 AND $t != 0) {
                $results = 10 + ( ( ($t - $b) - 1 ) * $diff);
            }
        } else {
            if ($b == $t) {
                $results = $t;
            } else if ($b < $t) {
                $results = $diff * -abs($t-$b);
            } else if ($b > $t) {
                $results = 1 * -abs($t-$b);
            } else if ($b == 0 AND $t == 0) {
                $results = 10;
            } else if ($b == 0 AND $t != 0) {
                $results = 10 + ( ( ($t - $b) - 1 ) * -1);
            }
        }

        return $results;
    }

    public function getTotalBids(){
        return count($this->bids) > 0 ? array_sum($this->bids) : 0 ;
    }

    public function getTypeGame(){
        return $this->getTotalBids() - 13 > 0 ? "Atas" : "Bawah" ;
    }

    public function getTotalDiff($total_bids){
        return abs($total_bids - 13);
    }

    public function getTrufPoint(){
        return $this->getTypeGame() . ' ' . $this->getTotalDiff($this->getTotalBids());
    }

}