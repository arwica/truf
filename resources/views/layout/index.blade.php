<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Truf Game</title>

    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <div class="container">

        <div class="col-sm-12">
            <form action="calculation" method="post" id="trufgame">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <fieldset>
                    <legend>Truf Game</legend>
                    <div class="col-sm-3">
                        <label for="exampleInputEmail1">North</label>
                        <div class="form-group">
                            <input type="text" name="bids[]" class="bids form-control" id="exampleInputEmail1" placeholder="Bid">
                        </div>
                        <div class="form-group">
                            <input type="text" name="tricks[]" class="tricks form-control" id="exampleInputPassword1" placeholder="Trick">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <label for="exampleInputEmail1">South</label>
                        <div class="form-group">
                            <input type="text" name="bids[]" class="bids form-control" id="exampleInputEmail1" placeholder="Bid">
                        </div>
                        <div class="form-group">
                            <input type="text" name="tricks[]" class="tricks form-control" id="exampleInputPassword1" placeholder="Trick">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <label for="exampleInputEmail1">West</label>
                        <div class="form-group">
                            <input type="text" name="bids[]" class="bids form-control" id="exampleInputEmail1" placeholder="Bid">
                        </div>
                        <div class="form-group">
                            <input type="text" name="tricks[]" class="tricks form-control" id="exampleInputPassword1" placeholder="Trick">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <label for="exampleInputEmail1">East</label>
                        <div class="form-group">
                            <input type="text" name="bids[]" class="bids form-control" id="exampleInputEmail1" placeholder="Bid">
                        </div>
                        <div class="form-group">
                            <input type="text" name="tricks[]" class="tricks form-control" id="exampleInputPassword1" placeholder="Trick">
                        </div>
                    </div>
                    <br/>

                    <button type="button" class="cal-game btn btn-default">Submit</button>
                    <button type="reset" class="btn btn-default">Reset Field</button>
                    <button type="button" onclick="return popupClearHistory();" class="btn btn-default">Clear History</button>
                </fieldset>
            </form>
            <br/>

            <legend>History Truf Game</legend>
            <table class="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>North</th>
                        <th>South</th>
                        <th>West</th>
                        <th>East</th>
                        <th>Point</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $x = 0;

                    $totalN = 0; $totalW = 0;
                    $totalS = 0; $totalE = 0;
                    if (!empty($history)) {
                        foreach ($history as $i => $row) {
                            $x++;

                            $totalN += $row['point_of_n'];
                            $totalS += $row['point_of_s'];
                            $totalW += $row['point_of_w'];
                            $totalE += $row['point_of_e'];

                            echo "<tr>
                                    <td>{$x}</td>
                                    <td>{$row['bid_of_n']} / {$row['tricks_of_n']} / {$row['point_of_n']}</td>
                                    <td>{$row['bid_of_s']} / {$row['tricks_of_s']} / {$row['point_of_s']}</td>
                                    <td>{$row['bid_of_w']} / {$row['tricks_of_w']} / {$row['point_of_w']}</td>
                                    <td>{$row['bid_of_e']} / {$row['tricks_of_e']} / {$row['point_of_e']}</td>
                                    <td>{$row['truf_point']}</td>
                                </tr>";
                        }
                    }
                    ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td>Total</td>
                        <td><?php echo $totalN; ?></td>
                        <td><?php echo $totalS; ?></td>
                        <td><?php echo $totalW; ?></td>
                        <td><?php echo $totalE; ?></td>
                        <td></td>
                    </tr>
                </tfoot>
            </table>
        </div>

    </div><!-- /.container -->


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript">
        function popupClearHistory(){
            var clear = confirm("Are you sure?");

            if (clear == true) {
                window.location.href = "/clear-history";
            }
        }

        $(document).ready(function(){
            $(".cal-game").click(function(){
                var bids = document.getElementsByClassName("bids");
                var tricks = document.getElementsByClassName("tricks");

                var total_bids = 0;
                var total_tricks = 0;

                for (var i = 0; i < bids.length; ++i) {
                    if (typeof bids[i].value !== "undefined") {
                        total_bids += parseInt(bids[i].value);
                    }
                }

                for (var i = 0; i < tricks.length; ++i) {
                    if (typeof tricks[i].value !== "undefined") {
                        total_tricks += parseInt(tricks[i].value);
                    }
                }

                if (total_bids != 13 && total_tricks <= 13) {
                    $("#trufgame").submit();
                } else {
                    if (total_bids == 13) {
                        alert("Total Bids " + total_bids);
                    } else if (total_tricks > 13) {
                        alert("Total Tricks must lower than 13, your tricks is " + total_tricks);
                    }
                }
            });
        });
    </script>
</body>
</html>