<?php

//use Illuminate\Foundation\Testing\WithoutMiddleware;
//use Illuminate\Foundation\Testing\DatabaseMigrations;
//use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Models\Truf;

class TrufGameTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    /*
     * @test
     */
    public function test_total_bid_atas(){
        $truf = new Truf(array(4,5,3,2), array(4,2,5,0));

        $results = $truf->getTotalBids();

        $this->assertEquals(14, $results);
    }

    /*
     * @test
     */
    public function test_total_bid_bawah(){
        $truf = new Truf(array(4,5,0,2), array(4,2,5,0));

        $results = $truf->getTotalBids();

        $this->assertEquals(11, $results);
    }

    /*
     * @test
     */
    public function test_truf_game_type_atas(){
        $truf = new Truf(array(4,5,3,2), array(4,2,5,0));

        $results = $truf->getTypeGame();

        $this->assertEquals("Atas", $results);
    }

    /*
     * @test
     */
    public function test_truf_game_type_bawah(){
        $truf = new Truf(array(4,5,0,2), array(4,2,5,0));

        $results = $truf->getTypeGame();

        $this->assertEquals("Bawah", $results);
    }

    /*
     * @test
     */
    public function test_truf_total_diff_atas(){
        $truf = new Truf(array(4,5,3,2), array(4,2,5,0));

        $results = $truf->getTotalDiff($truf->getTotalBids());

        $this->assertEquals(1, $results);
    }

    /*
     * @test
     */
    public function test_truf_total_diff_bawah(){
        $truf = new Truf(array(4,5,0,2), array(4,2,5,0));

        $results = $truf->getTotalDiff($truf->getTotalBids());

        $this->assertEquals(2, $results);
    }

    /*
     * @test
     */
    public function test_total_truf_point_atas(){
        $truf = new Truf(array(4,5,3,2), array(4,2,5,0));

        $results = $truf->getTrufPoint();

        $this->assertEquals("Atas 1", $results);
    }

    /*
     * @test
     */
    public function test_total_truf_point_bawah(){
        $truf = new Truf(array(3,7,0,1), array(2,6,2,2));

        $results = $truf->getTrufPoint();

        $this->assertEquals("Bawah 2", $results);
    }

    /*
     * @test
     */
    public function test_truf_point_pp_atas_1(){
        $truf = new Truf(array(4,5,3,2), array(4,2,5,0));

        $results = $truf->perPlayerScoreCalculation(4, 4, $truf->getTotalBids());

        $this->assertEquals(4, $results);
    }

    /*
     * @test
     */
    public function test_truf_point_pp_atas_2(){
        $truf = new Truf(array(4,5,3,2), array(4,2,5,0));

        $results = $truf->perPlayerScoreCalculation(5, 2, $truf->getTotalBids());

        $this->assertEquals(-3, $results);
    }

    /*
     * @test
     */
    public function test_truf_point_pp_atas_3(){
        $truf = new Truf(array(4,5,3,2), array(4,2,5,0));

        $results = $truf->perPlayerScoreCalculation(3, 5, $truf->getTotalBids());

        $this->assertEquals(-2, $results);
    }

    /*
     * @test
     */
    public function test_truf_point_pp_atas_4(){
        $truf = new Truf(array(4,5,3,2), array(4,2,5,0));

        $results = $truf->perPlayerScoreCalculation(2, 0, $truf->getTotalBids());

        $this->assertEquals(-2, $results);
    }

    /*
     * @test
     */
    public function test_truf_point_pp_bawah_1(){
        $truf = new Truf(array(3,7,0,1), array(2,6,2,2));

        $results = $truf->perPlayerScoreCalculation(3, 2, $truf->getTotalBids());

        $this->assertEquals(-1, $results);
    }

    /*
     * @test
     */
    public function test_truf_point_pp_bawah_2(){
        $truf = new Truf(array(3,7,0,1), array(2,6,2,2));

        $results = $truf->perPlayerScoreCalculation(7, 6, $truf->getTotalBids());

        $this->assertEquals(-1, $results);
    }

    /*
     * @test
     */
    public function test_truf_point_pp_bawah_3(){
        $truf = new Truf(array(3,7,0,1), array(2,6,2,2));

        $results = $truf->perPlayerScoreCalculation(0, 2, $truf->getTotalBids());

        $this->assertEquals(-4, $results);
    }

    /*
     * @test
     */
    public function test_truf_point_pp_bawah_4(){
        $truf = new Truf(array(3,7,0,1), array(2,6,2,2));

        $results = $truf->perPlayerScoreCalculation(1, 2, $truf->getTotalBids());

        $this->assertEquals(-2, $results);
    }
}
